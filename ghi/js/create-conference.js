document.addEventListener('DOMContentLoaded', async () => {

    const form = document.getElementById('new-conference-form');
    form.addEventListener('submit', async function (event) {
        event.preventDefault();

        const formData = {
            name: document.getElementById('name').value,
            starts: document.getElementById('starts').value,
            ends: document.getElementById('ends').value,
            description: document.getElementById('description').value,
            max_presentations: parseInt(document.getElementById('max_presentations').value, 10),
            max_attendees: parseInt(document.getElementById('max_attendees').value, 10),
            location: parseInt(document.getElementById('location').value, 10)
        };

        try {
            const response = await fetch('http://localhost:8000/api/conferences/', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(formData)
            });
            if (!response.ok) {
                throw new Error('Failed to create conference');
            }
            const result = await response.json();
            console.log('Conference created:', result);
            form.reset();
        } catch (error) {
            console.error('Error:', error.message);
        }
    });

    try {
        const response = await fetch('http://localhost:8000/api/locations/');
        if (!response.ok) {
            throw new Error('Failed to fetch locations');
        }
        const data = await response.json();
        const locationSelect = document.getElementById('location');
        data.locations.map(location => {
            const option = document.createElement('option');
            option.value = location.id;
            option.textContent = location.name;
            locationSelect.appendChild(option);
    });
    } 
    catch (error) {
        console.error('Error:', error.message);
    }
});


