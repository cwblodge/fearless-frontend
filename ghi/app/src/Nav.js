import React from 'react';
import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <NavLink className="navbar-brand" to="/">Conference GO!</NavLink>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink className="nav-link" to="/" exact="Home">Home</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/conferences/new">New Conference</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/attendees/new">New Attendee</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/locations/new">New Location</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/attendees">Attendees List</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/presentations/new">New Presentation</NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Nav;







