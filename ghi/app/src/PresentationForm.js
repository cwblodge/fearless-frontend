import React, { useEffect, useState } from 'react';

const NewPresentation = () => {
  const [conferences, setConferences] = useState([]);

  useEffect(() => {
    const fetchConferences = async () => {
      try {
        const response = await fetch('http://localhost:8000/api/conferences/');
        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences);
        } else {
          throw new Error('Failed to fetch conferences');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    };

    fetchConferences();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const json = JSON.stringify(Object.fromEntries(formData));
    const conferenceId = formData.get('conference');
    const locationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;

    try {
      const response = await fetch(locationUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: json,
      });

      if (response.ok) {
        event.target.reset();
        const newConference = await response.json();
        console.log('Presentation created:', newConference);
      } else {
        throw new Error('Failed to create presentation');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form id="create-presentation-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
              </div>
              <div className="mb-3">
                <select required name="conference" id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return <option key={conference.id} value={conference.id}>{conference.name}</option>
                  })}
                </select>
              </div>
              <button type="submit" className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewPresentation;




